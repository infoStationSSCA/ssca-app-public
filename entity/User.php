<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\CommonInterface\CreatedAtInterface;
use App\CommonInterface\UpdatedAtInterface;
use App\CommonTrait\CreatedAtTrait;
use App\CommonTrait\UpdatedAtTrait;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, CreatedAtInterface, UpdatedAtInterface
{
    use CreatedAtTrait;
    use UpdatedAtTrait;

    public const PHOTO_MIN_WIDTH = 520;
    public const PHOTO_MAX_WIDTH = 960;
    public const PHOTO_MIN_HEIGHT = 520;
    public const PHOTO_MAX_HEIGHT = 960;
    public const PHOTO_MAX_SIZE = '2000k';

    public const QUIZ_STATUS_NOT_ANSWERED = 1;
    public const QUIZ_STATUS_OK = 2;
    public const QUIZ_STATUS_KO = 3;
    public const QUIZ_STATUS_EXPIRED = 4;
    public const QUIZ_STATUS_BLACKLISTED = 5;
    public const USER_PER_PAGE = 50;
    public const USER_API_PER_PAGE = 100;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $idMongoDB;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $passwordToken;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Assert\Choice(callback={"App\Service\ChoiceService", "getIndicatorsValidator"})
     */
    private $indicator;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Assert\Regex(pattern="/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/")
     */
    private $phone;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\IsTrue()
     */
    private $hasAcceptedCondition;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $contactLanguage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Range(
     *      min = 100000000,
     *      max = 4294967295,
     * )
     */
    private $ic;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEnabled = false;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity=UserQuiz::class, mappedBy="user", cascade={"remove"})
     */
    private $userQuizzes;

    /**
     * @ORM\OneToMany(targetEntity=UserMessage::class, mappedBy="user", cascade={"remove"})
     */
    private $userMessages;

    /**
     * @ORM\OneToMany(targetEntity=CameraUserTracking::class, mappedBy="user", cascade={"remove"})
     */
    private $cameraUserTrackings;

    /**
     * @ORM\OneToMany(targetEntity=CameraUser::class, mappedBy="user", cascade={"remove"})
     */
    private $cameraUsers;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    private $isBlacklisted;

    /**
     * @ORM\OneToMany(targetEntity=UserQuizGroupListener::class, mappedBy="user", cascade={"remove"})
     */
    private $userQuizGroupListeners;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $reciveSms = false;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $group;

    /**
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    private $quizStatus = self::QUIZ_STATUS_NOT_ANSWERED;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity=QrScannerUserTracking::class, mappedBy="user", cascade={"remove"})
     */
    private $qrScannerUserTrackings;

    /**
     * @ORM\OneToMany(targetEntity=UserPollInvitation::class, mappedBy="user", cascade={"remove"})
     */
    private $userPollInvitations;

    /**
     * @ORM\OneToMany(targetEntity=UserPoll::class, mappedBy="user", cascade={"remove"})
     */
    private $userPolls;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $mondayEmailTimeSent;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $tuesdayEmailTimeSent;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $wednesdayEmailTimeSent;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $thursdayEmailTimeSent;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $fridayEmailTimeSent;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $saturdayEmailTimeSent;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $sundayEmailTimeSent;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $authedWithSso = false;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $birthDateVaccinCode;

    public function __construct()
    {
        $this->userQuizzes = new ArrayCollection();
        $this->userMessages = new ArrayCollection();
        $this->cameraUserTrackings = new ArrayCollection();
        $this->cameraUsers = new ArrayCollection();
        $this->userQuizGroupListeners = new ArrayCollection();
        $this->qrScannerUserTrackings = new ArrayCollection();
        $this->userPollInvitations = new ArrayCollection();
        $this->userPolls = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdMongoDB(): ?string
    {
        return $this->idMongoDB;
    }

    public function setIdMongoDB(string $idMongoDB): self
    {
        $this->idMongoDB = $idMongoDB;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = str_replace("'","",$firstName);

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = str_replace("'","",$lastName);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPasswordToken(): ?string
    {
        return $this->passwordToken;
    }

    public function setPasswordToken(?string $passwordToken): self
    {
        $this->passwordToken = $passwordToken;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles()
    {
        $roles = $this->roles;
        // give everyone ROLE_USER!
        if (!in_array('ROLE_USER', $roles)) {
            $roles[] = 'ROLE_USER';
        }
        return $roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }


    public function getIndicator(): ?string
    {
        return $this->indicator;
    }

    public function setIndicator(?string $indicator): self
    {
        $this->indicator = $indicator;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getHasAcceptedCondition(): ?bool
    {
        return $this->hasAcceptedCondition;
    }

    public function setHasAcceptedCondition(bool $hasAcceptedCondition): self
    {
        $this->hasAcceptedCondition = $hasAcceptedCondition;

        return $this;
    }

    public function getContactLanguage(): ?string
    {
        return $this->contactLanguage;
    }

    public function setContactLanguage(string $contactLanguage): self
    {
        $this->contactLanguage = $contactLanguage;

        return $this;
    }

    public function getQr(): ?string
    {
        return $this->qr;
    }

    public function setQr(string $qr): self
    {
        $this->qr = $qr;

        return $this;
    }

    public function getIc(): ?string
    {
        return $this->ic;
    }

    public function setIc(?string $ic): self
    {
        $this->ic = $ic;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getAuthedWithSso(): ?bool
    {
        return $this->authedWithSso;
    }

    public function setAuthedWithSso(bool $authedWithSso): self
    {
        $this->authedWithSso = $authedWithSso;

        return $this;
    }
    public function getBirthDateVaccinCode(): ?string
    {
        return $this->birthDateVaccinCode;
    }

    public function setBirthDateVaccinCode(string $birthDateVaccinCode): self
    {
        $this->birthDateVaccinCode = $birthDateVaccinCode;
        return $this;
    }
    /**
     * @return Collection|UserQuiz[]
     */
    public function getUserQuizzes(): Collection
    {
        return $this->userQuizzes;
    }

    public function addUserQuiz(UserQuiz $userQuiz): self
    {
        if (!$this->userQuizzes->contains($userQuiz)) {
            $this->userQuizzes[] = $userQuiz;
            $userQuiz->setUser($this);
        }

        return $this;
    }

    public function removeUserQuiz(UserQuiz $userQuiz): self
    {
        if ($this->userQuizzes->contains($userQuiz)) {
            $this->userQuizzes->removeElement($userQuiz);
            // set the owning side to null (unless already changed)
            if ($userQuiz->getUser() === $this) {
                $userQuiz->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserMessage[]
     */
    public function getUserMessages(): Collection
    {
        return $this->userMessages;
    }

    public function addUserMessage(UserMessage $userMessage): self
    {
        if (!$this->userMessages->contains($userMessage)) {
            $this->userMessages[] = $userMessage;
            $userMessage->setUser($this);
        }

        return $this;
    }

    public function removeUserMessage(UserMessage $userMessage): self
    {
        if ($this->userMessages->contains($userMessage)) {
            $this->userMessages->removeElement($userMessage);
            // set the owning side to null (unless already changed)
            if ($userMessage->getUser() === $this) {
                $userMessage->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CameraUserTracking[]
     */
    public function getCameraUserTrackings(): Collection
    {
        return $this->cameraUserTrackings;
    }

    public function addCameraUserTracking(CameraUserTracking $cameraUserTracking): self
    {
        if (!$this->cameraUserTrackings->contains($cameraUserTracking)) {
            $this->cameraUserTrackings[] = $cameraUserTracking;
            $cameraUserTracking->setUser($this);
        }

        return $this;
    }

    public function removeCameraUserTracking(CameraUserTracking $cameraUserTracking): self
    {
        if ($this->cameraUserTrackings->contains($cameraUserTracking)) {
            $this->cameraUserTrackings->removeElement($cameraUserTracking);
            // set the owning side to null (unless already changed)
            if ($cameraUserTracking->getUser() === $this) {
                $cameraUserTracking->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CameraUser[]
     */
    public function getCameraUsers(): Collection
    {
        return $this->cameraUsers;
    }

    public function addCameraUser(CameraUser $cameraUser): self
    {
        if (!$this->cameraUsers->contains($cameraUser)) {
            $this->cameraUsers[] = $cameraUser;
            $cameraUser->setUser($this);
        }

        return $this;
    }

    public function removeCameraUser(CameraUser $cameraUser): self
    {
        if ($this->cameraUsers->contains($cameraUser)) {
            $this->cameraUsers->removeElement($cameraUser);
            // set the owning side to null (unless already changed)
            if ($cameraUser->getUser() === $this) {
                $cameraUser->setUser(null);
            }
        }

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getIsBlacklisted(): ?bool
    {
        return $this->isBlacklisted;
    }

    public function setIsBlacklisted(?bool $isBlacklisted): self
    {
        $this->isBlacklisted = $isBlacklisted;

        return $this;
    }

    /**
     * @return Collection|UserQuizGroupListener[]
     */
    public function getUserQuizGroupListeners(): Collection
    {
        return $this->userQuizGroupListeners;
    }

    public function addUserQuizGroupListener(UserQuizGroupListener $userQuizGroupListener): self
    {
        if (!$this->userQuizGroupListeners->contains($userQuizGroupListener)) {
            $this->userQuizGroupListeners[] = $userQuizGroupListener;
            $userQuizGroupListener->setUser($this);
        }

        return $this;
    }

    public function removeUserQuizGroupListener(UserQuizGroupListener $userQuizGroupListener): self
    {
        if ($this->userQuizGroupListeners->contains($userQuizGroupListener)) {
            $this->userQuizGroupListeners->removeElement($userQuizGroupListener);
            // set the owning side to null (unless already changed)
            if ($userQuizGroupListener->getUser() === $this) {
                $userQuizGroupListener->setUser(null);
            }
        }

        return $this;
    }

    public function getReciveSms(): ?bool
    {
        return $this->reciveSms;
    }

    public function setReciveSms(?bool $reciveSms): self
    {
        $this->reciveSms = $reciveSms;

        return $this;
    }

    public function getGroup(): ?Group
    {
        return $this->group;
    }

    public function setGroup(?Group $group): self
    {
        $this->group = $group;

        return $this;
    }
    public function __toString()
    {
        return (string) $this->getFirstName();
    }

    public function getQuizStatus(): ?int
    {
        return $this->quizStatus;
    }

    public function setQuizStatus(int $quizStatus): self
    {
        $this->quizStatus = $quizStatus;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|QrScannerUserTracking[]
     */
    public function getQrScannerUserTrackings(): Collection
    {
        return $this->qrScannerUserTrackings;
    }

    public function addQrScannerUserTracking(QrScannerUserTracking $qrScannerUserTracking): self
    {
        if (!$this->qrScannerUserTrackings->contains($qrScannerUserTracking)) {
            $this->qrScannerUserTrackings[] = $qrScannerUserTracking;
            $qrScannerUserTracking->setUser($this);
        }

        return $this;
    }

    public function removeQrScannerUserTracking(QrScannerUserTracking $qrScannerUserTracking): self
    {
        if ($this->qrScannerUserTrackings->contains($qrScannerUserTracking)) {
            $this->qrScannerUserTrackings->removeElement($qrScannerUserTracking);
            // set the owning side to null (unless already changed)
            if ($qrScannerUserTracking->getUser() === $this) {
                $qrScannerUserTracking->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPollInvitation[]
     */
    public function getUserPollInvitations(): Collection
    {
        return $this->userPollInvitations;
    }

    public function addUserPollInvitation(UserPollInvitation $userPollInvitation): self
    {
        if (!$this->userPollInvitations->contains($userPollInvitation)) {
            $this->userPollInvitations[] = $userPollInvitation;
            $userPollInvitation->setUser($this);
        }

        return $this;
    }

    public function removeUserPollInvitation(UserPollInvitation $userPollInvitation): self
    {
        if ($this->userPollInvitations->contains($userPollInvitation)) {
            $this->userPollInvitations->removeElement($userPollInvitation);
            // set the owning side to null (unless already changed)
            if ($userPollInvitation->getUser() === $this) {
                $userPollInvitation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPoll[]
     */
    public function getUserPolls(): Collection
    {
        return $this->userPolls;
    }

    public function addUserPoll(UserPoll $userPoll): self
    {
        if (!$this->userPolls->contains($userPoll)) {
            $this->userPolls[] = $userPoll;
            $userPoll->setUser($this);
        }

        return $this;
    }

    public function removeUserPoll(UserPoll $userPoll): self
    {
        if ($this->userPolls->contains($userPoll)) {
            $this->userPolls->removeElement($userPoll);
            // set the owning side to null (unless already changed)
            if ($userPoll->getUser() === $this) {
                $userPoll->setUser(null);
            }
        }

        return $this;
    }

    public function getMondayEmailTimeSent(): ?string
    {
        return $this->mondayEmailTimeSent;
    }

    public function setMondayEmailTimeSent(?string $mondayEmailTimeSent): self
    {
        $this->mondayEmailTimeSent = $mondayEmailTimeSent;

        return $this;
    }

    public function getTuesdayEmailTimeSent(): ?string
    {
        return $this->tuesdayEmailTimeSent;
    }

    public function setTuesdayEmailTimeSent(?string $tuesdayEmailTimeSent): self
    {
        $this->tuesdayEmailTimeSent = $tuesdayEmailTimeSent;

        return $this;
    }

    public function getWednesdayEmailTimeSent(): ?string
    {
        return $this->wednesdayEmailTimeSent;
    }

    public function setWednesdayEmailTimeSent(?string $wednesdayEmailTimeSent): self
    {
        $this->wednesdayEmailTimeSent = $wednesdayEmailTimeSent;

        return $this;
    }

    public function getThursdayEmailTimeSent(): ?string
    {
        return $this->thursdayEmailTimeSent;
    }

    public function setThursdayEmailTimeSent(?string $thursdayEmailTimeSent): self
    {
        $this->thursdayEmailTimeSent = $thursdayEmailTimeSent;

        return $this;
    }

    public function getFridayEmailTimeSent(): ?string
    {
        return $this->fridayEmailTimeSent;
    }

    public function setFridayEmailTimeSent(?string $fridayEmailTimeSent): self
    {
        $this->fridayEmailTimeSent = $fridayEmailTimeSent;

        return $this;
    }

    public function getSaturdayEmailTimeSent(): ?string
    {
        return $this->saturdayEmailTimeSent;
    }

    public function setSaturdayEmailTimeSent(?string $saturdayEmailTimeSent): self
    {
        $this->saturdayEmailTimeSent = $saturdayEmailTimeSent;

        return $this;
    }

    public function getSundayEmailTimeSent(): ?string
    {
        return $this->sundayEmailTimeSent;
    }

    public function setSundayEmailTimeSent(?string $sundayEmailTimeSent): self
    {
        $this->sundayEmailTimeSent = $sundayEmailTimeSent;

        return $this;
    }
}
