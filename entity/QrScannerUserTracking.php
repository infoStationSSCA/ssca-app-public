<?php

namespace App\Entity;

use App\CommonInterface\CreatedAtInterface;
use App\CommonInterface\UpdatedAtInterface;
use App\CommonTrait\CreatedAtTrait;
use App\CommonTrait\UpdatedAtTrait;
use App\Repository\QrScannerUserTrackingRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=QrScannerUserTrackingRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class QrScannerUserTracking implements CreatedAtInterface, UpdatedAtInterface
{
    public const QR_SCANNER_USER_TRACKING_PAGE = 50;
    public const QR_SCANNER_USER_TRACKING_API_PAGE = 100;

    use CreatedAtTrait;
    use UpdatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $userEmail;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $userFirstName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $userLastName;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $userIndicator;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Assert\Regex(pattern="/^([1-9][0-9]*)$/")
     */
    private $userPhone;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="qrScannerUserTrackings")
     * @ORM\JoinColumn(nullable=true)
     */
    private $userGroup;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qrScannerName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qrScannerAddress;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $qrScannerType;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isGranted;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="qrScannerUserTrackings")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="qrScannerUserTrackings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity=QrScanner::class, inversedBy="qrScannerUserTrackings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $qrScanner;

    /**
     * @ORM\ManyToOne(targetEntity=UserQuiz::class, inversedBy="cameraUserTrackings")
     */
    private $userQuiz;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $quizStatus;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $ScannerExistGroup;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idTabletteQuizResponse;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isVaccinationQrValid = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setUserEmail(?string $userEmail): self
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    public function getUserFirstName(): ?string
    {
        return $this->userFirstName;
    }

    public function setUserFirstName(?string $userFirstName): self
    {
        $this->userFirstName = $userFirstName;

        return $this;
    }

    public function getUserLastName(): ?string
    {
        return $this->userLastName;
    }

    public function setUserLastName(?string $userLastName): self
    {
        $this->userLastName = $userLastName;

        return $this;
    }

    public function getUserIndicator(): ?string
    {
        return $this->userIndicator;
    }

    public function setUserIndicator(?string $userIndicator): self
    {
        $this->userIndicator = $userIndicator;

        return $this;
    }

    public function getUserPhone(): ?string
    {
        return $this->userPhone;
    }

    public function setUserPhone(?string $userPhone): self
    {
        $this->userPhone = $userPhone;

        return $this;
    }

    public function getUserGroup(): ?Group
    {
        return $this->userGroup;
    }

    public function setUserGroup(?Group $userGroup): self
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    public function getQrScannerName(): ?string
    {
        return $this->qrScannerName;
    }

    public function setQrScannerName(?string $qrScannerName): self
    {
        $this->qrScannerName = $qrScannerName;

        return $this;
    }

    public function getQrScannerAddress(): ?string
    {
        return $this->qrScannerAddress;
    }

    public function setQrScannerAddress(?string $qrScannerAddress): self
    {
        $this->qrScannerAddress = $qrScannerAddress;

        return $this;
    }

    public function getQrScannerType(): ?string
    {
        return $this->qrScannerType;
    }

    public function setQrScannerType(?string $qrScannerType): self
    {
        $this->qrScannerType = $qrScannerType;

        return $this;
    }

    public function getIsGranted(): ?bool
    {
        return $this->isGranted;
    }

    public function setIsGranted(?bool $isGranted): self
    {
        $this->isGranted = $isGranted;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getQrScanner(): ?QrScanner
    {
        return $this->qrScanner;
    }

    public function setQrScanner(?QrScanner $qrScanner): self
    {
        $this->qrScanner = $qrScanner;

        return $this;
    }

    public function getUserQuiz(): ?UserQuiz
    {
        return $this->userQuiz;
    }

    public function setUserQuiz(?UserQuiz $userQuiz): self
    {
        $this->userQuiz = $userQuiz;

        return $this;
    }

    public function getQuizStatus(): ?int
    {
        return $this->quizStatus;
    }

    public function setQuizStatus(int $quizStatus): self
    {
        $this->quizStatus = $quizStatus;

        return $this;
    }

    public function getScannerExistGroup(): ?bool
    {
        return $this->ScannerExistGroup;
    }

    public function setScannerExistGroup(bool $ScannerExistGroup): self
    {
        $this->ScannerExistGroup = $ScannerExistGroup;

        return $this;
    }

    public function getIdTabletteQuizResponse(): ?int
    {
        return $this->quizStatus;
    }

    public function setIdTabletteQuizResponse(int $idTabletteQuizResponse): self
    {
        $this->idTabletteQuizResponse = $idTabletteQuizResponse;

        return $this;
    }

    public function getIsVaccinationQrValid(): ?bool
    {
        return $this->isVaccinationQrValid;
    }

    public function setIsVaccinationQrValid(?bool $isVaccinationQrValid): self
    {
        $this->isVaccinationQrValid = $isVaccinationQrValid;

        return $this;
    }
}
