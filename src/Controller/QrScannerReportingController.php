<?php

namespace App\Controller\Admin;

use App\Entity\QrScannerUserTracking;
use App\Entity\User;
use App\Repository\QrScannerUserTrackingRepository;
use App\Repository\GroupRepository;
use App\Repository\QrScannerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Response;

class QrScannerReportingController extends AbstractController
{
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/qr-scanner-reporting", name="admin_qr_scanner_reporting")
     */
    public function index(
        Request $request,
        GroupRepository $groupRepository,
        QrScannerRepository $qrScannerRepository,
        QrScannerUserTrackingRepository $qrScannerUserTrackingRepository
    ) {
        [$scannerReports, $scannerReportsCount] = $qrScannerUserTrackingRepository->findByFilter(
            $this->getUser()->getCompany(),
            $request->get('type', ''),
            $request->get('group', ''),
            $request->get('status', ''),
            $request->get('scanner_exist_group', ''),
            $request->get('quiz_status', ''),
            $request->get('sanner', ''),
            $request->get('is_vaccination_qr_valid', ''),
            $request->get('is_granted', ''),
            $request->get('from', date('Y-m-d H:i:s', strtotime('-24 hours'))),
            $request->get('to', date('Y-m-d H:i:s')),
            $request->get('search', ''),
            $request->get('page', 1)
        );
        // die(print_r($scannerReports[0]['userGroup']));

        return $this->render('admin/qr_scanner_reporting/index.html.twig', [
            'scannerReports' => $scannerReports,
            'groups' => $groupRepository->findBy(['company' => $this->getUser()->getCompany(), 'isArchived' => false]),
            'pages' => (int) ceil(($scannerReportsCount) / 25),
            'statusQuiz' => $this->getStatusQuiz(),
            'scanners' => $qrScannerRepository->findQrScannerName($this->getUser()->getCompany()),
            'company' => $this->getUser()->getCompany()
        ]);
    }

    /**
     * @Route("/qr-scanner-reporting/json", name="admin_qr_scanner_reporting_json")
     */

    public function indexJson(
        Request $request,
        GroupRepository $groupRepository,
        QrScannerRepository $qrScannerRepository,
        QrScannerUserTrackingRepository $qrScannerUserTrackingRepository
    ) {
        [$scannerReports, $scannerReportsCount] = $qrScannerUserTrackingRepository->findByFilter(
            $this->getUser()->getCompany(),
            $request->get('type', ''),
            $request->get('group', ''),
            $request->get('status', ''),
            $request->get('scanner_exist_group', ''),
            $request->get('quiz_status', ''),
            $request->get('sanner', ''),
            $request->get('is_vaccination_qr_valid', ''),            
            $request->get('is_granted', ''),            
            $request->get('from', date('Y-m-d H:i:s', strtotime('-24 hours'))),
            $request->get('to', date('Y-m-d H:i:s')),
            $request->get('search', ''),
            $request->get('page', 1)
        );
        $response = new Response(json_encode(array('name' => $scannerReports)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function getStatusQuiz()
    {
        $stausQuiz[$this->translator->trans('QUIZ_STATUS_NOT_ANSWERED')] = User::QUIZ_STATUS_NOT_ANSWERED;
        $stausQuiz[$this->translator->trans('QUIZ_STATUS_OK')] = User::QUIZ_STATUS_OK;
        $stausQuiz[$this->translator->trans('QUIZ_STATUS_KO')] = User::QUIZ_STATUS_KO;
        $stausQuiz[$this->translator->trans('QUIZ_STATUS_EXPIRED')] = User::QUIZ_STATUS_EXPIRED;
        $stausQuiz[$this->translator->trans('QUIZ_STATUS_BLACKLISTED')] = User::QUIZ_STATUS_BLACKLISTED;
        return $stausQuiz;
    }
}
