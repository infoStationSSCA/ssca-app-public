<?php

namespace App\Controller\Webhook;

use App\Entity\ActionLog;
use App\Entity\Company;
use App\Entity\CompanySetting;
use App\Entity\QrScanner;
use App\Entity\QrScannerUserTracking;
use App\Entity\User;
use App\Entity\UserQuiz;
use App\Repository\CompanySettingRepository;
use App\Repository\QrScannerRepository;
use App\Repository\UserQuizRepository;
use App\Repository\UserRepository;
use App\Repository\QuizRepository;
use App\Repository\GroupRepository;
use App\Repository\UserQuizGroupListenerRepository;
use App\Service\EmailService;
use App\Service\UserQuizService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PaireQrScannerTabletteRepository;
use App\Repository\TabletteQuizResponseRepository;
use App\Entity\TabletteQuizResponse;
use DateTime;

class QrScannerController extends AbstractController
{

    protected $qrScannerRepository;
    private $em;

    public function __construct(QrScannerRepository $qrScannerRepository, EntityManagerInterface $em)
    {
        $this->qrScannerRepository = $qrScannerRepository;
        $this->em = $em;
    }

    /**
     * @Route("/webhook/qr-scanner/check-access", name="webhook_qr_scanner_check_access", methods={"POST"})
     */
    public function checkAccess(
        Request $request,
        QrScannerRepository $qrScannerRepository,
        UserRepository $userRepository,
        CompanySettingRepository $companySettingRepository,
        UserQuizRepository $userQuizRepository,
        EmailService $emailService,
        LoggerInterface $logger,
        PaireQrScannerTabletteRepository $paireQrScannerTabletteRepository,
        TabletteQuizResponseRepository $tabletteQuizResponseRepository,
        QuizRepository $quizRepository,
        UserQuizService $userQuizService,
        UserQuizGroupListenerRepository $userQuizGroupListenerRepository,
        GroupRepository $groupRepository
    ) {
        $isVaccinationQr = strlen($request) > 1000;

        $pairedTablet = null;
        $vaccinValidity = null;

        $parameters = $this->formatRequestParameters($request);

        if (!$parameters) return new Response('code=0000');

        $deviceId = $parameters['devicenumber'] ?? null;

        $ic = $parameters['vgdecoderesult'] ?? null;

        if (!$deviceId) return new Response('code=0000');

        $qrScanner = $qrScannerRepository->findOneBy(['deviceId' => $deviceId]);

        if (!$qrScanner) { // if the QR scanner does not exist in the database
            if ($ic === $this->getParameter('qrCodeForEquipment')) {
                $this->createQrScanner($deviceId); // we create a new QR scanner
                return new Response('code=0000');
            }
            return new Response('code=0001');
        }

        // HERE we know the QR scanner is defined
        $pairedTablet = $paireQrScannerTabletteRepository->findOneBy(['idQrScanner' => $qrScanner->getDeviceId()]);
        $company = $qrScanner->getCompany(); // we find the company associated with the QR scanner
        if (!$company) { // if there is not company associated with the QR scanner, we return an error
            $logger->info('A QR code has been scanned on an unassociated QR reader');
            return new Response('code=0001');
        }
        $companySetting = $companySettingRepository->findOneBy(['company' => $company]);
        $groupe = $this->em->getRepository('App:Group')->findOneBy(['company' => $company]);

        // if the QR code scanned is a Vaccination QR Code
        if ($isVaccinationQr) {
            if (!$company->getVaccinApiEnabled()) {
                $logger->info('A vaccination QR code has been scanned while the Vaccination API or Mode Pandemic was disabled');
                return new Response('code=0001');
            }

            $clientVaccin = HttpClient::create();
            $responseVaccin = $clientVaccin->request('POST', $this->getParameter('vaccinationUrl') . '/qr-scan/decriptobj', [
                'headers' => [
                    'Content-Type' => 'text/plain',
                ],
                'body' => $request->getContent(),
            ]);
            try {
                // retrieving vaccination API response
                $firstName = str_replace("'", "", $responseVaccin->toArray()["name"]);
                $lastName = str_replace("'", "", $responseVaccin->toArray()["familyName"]);
                $birthdate = $responseVaccin->toArray()["birthdate"];
                $vaccinValidity = $responseVaccin->toArray()['garanted']; // Whether or not the vaccination QR code is valid

                // if we have a Vaccination QR code, we find the user by it's firstname, lastname and birthdate
                $user = $userRepository->findOneBy(['firstName' => $firstName, 'lastName' => $lastName, 'birthDateVaccinCode' => $birthdate, 'company' => $company]);

                // if don't find any user, we create a new one using the vaccination QR code information
                if (!$user) $user = $this->createUserUsingVaccinQR($groupe, $firstName, $lastName, $company, $birthdate);
            } catch (Exception $e) {
                $logger->error("An exception occured while retrieving the user's vaccination information : " . $e);
                return new Response('code=0001');
            }
        } else {
            // we find the user associated with the IC number scanned by the QR scanner
            $user = $userRepository->findOneBy(['ic' => $ic, 'company' => $company]);
        }
        $userQuiz = $user ? $userQuizRepository->findOneBy(['user' => $user, 'isLast' => true]) : null;
     
        // if the QR reader is paired with a tablet & a quiz has been completed
        if ($pairedTablet != null && $pairedTablet->getTabletteSentData()) {
            // we indicate there has been a QR code scanned on the QR scanner associated with the tablet
            $pairedTablet->setQrScanned(true);
            $this->em->merge($pairedTablet);
            $this->em->flush();
            if ($user) {
                $pairedTablet->setIsRecognisedQrCode(true);
                $this->em->merge($pairedTablet);
                $this->em->flush();
            } else {
                $pairedTablet->setIsRecognisedQrCode(false);
                $this->em->merge($pairedTablet);
                $this->em->flush();
                return new Response('code=0001');
            }
           
            if ($pairedTablet->getUserHaveQrCode()) {
                $this->updateQuizStatusPairedTablet($pairedTablet->getTabletteCompliantQuiz(), $user);
            }
        }


        // we check if the user should have it's access granted or revoked
        $isGrantedUser = $this->isGrantedUser($qrScanner, $user, $companySetting, $isVaccinationQr, $vaccinValidity);

        // we create the QrScannerUserTracking
        $qrScannerUserTracking = $this->createQrScannerUserTracking($qrScanner, $userQuiz, $user, $isGrantedUser, $vaccinValidity); // we create a QR scanner tracking

        if ($pairedTablet != null && $pairedTablet->getTabletteSentData() && $pairedTablet->getUserHaveQrCode()) { // if the QR reader is paired with a tablet & a quiz has been completed
            // We modify the tabletteQuizResponseAnjoute Entity to link the Quiz with the User but also with the QrScannerUserTracking
            $tabletteQuizResponse = $tabletteQuizResponseRepository->findOneBy(['idQrScanner' => $parameters['devicenumber']], ['id' => 'DESC']);
            $this->linkUserWithTabletteQuizResponse($user, $tabletteQuizResponse, $qrScannerUserTracking, $userQuizService, $pairedTablet, $quizRepository, $groupRepository);
            $isGrantedUser = $this->isGrantedUser($qrScanner, $user, $companySetting, $isVaccinationQr, $vaccinValidity);
            $this->updateQrScannerUserTracking($qrScannerUserTracking, $qrScanner, $userQuiz, $user, $tabletteQuizResponse, $isGrantedUser);
        } else if ($user && ($user->getQuizStatus() === User::QUIZ_STATUS_OK || $user->getQuizStatus() === User::QUIZ_STATUS_KO)) {
            // if there is a scan on QR reader not associated with a tablet OR there was no answer given on the tablet
            // we check if the user's quiz status is OK or KO. 
            // if that's the case, we find the last quiz of the user and associate it with the QrScannerUserTracking
            $tabletteQuizResponse = $tabletteQuizResponseRepository->findOneBy(['idUser' => $user->getId()], ['id' => 'DESC']);
            $qrScannerUserTracking->setIdTabletteQuizResponse($tabletteQuizResponse->getId());
            $this->em->persist($qrScannerUserTracking);
            $this->em->flush();
        }

        // Email Reporting...
        if ($userQuiz && $userQuiz->getReportRecipientEmails() && $companySetting->getEmailReporting() && $companySetting->getAllowSendEmail()) {
            $request->setLocale($user->getContactLanguage());
            $userComplementaryQuizzes = $userQuizRepository->findBy(['parentUserQuiz' => $userQuiz]);
            $emailService->sendReport($qrScannerUserTracking, $userQuiz, $userComplementaryQuizzes);
        }

        // Send email notification to the managers subscribing to a Qr scan event
        if ($user) {
            $this->sendEmailSubscribersQrScan($isGrantedUser, $userQuizGroupListenerRepository, $groupe, $companySetting, $emailService, $user, $qrScanner);
        }

        if (!$isGrantedUser) {
            return new Response('code=0001');
        }


        return new Response('code=0000');
    }

    private function formatRequestParameters(Request $request)
    {
        $requestParameters = explode('&&', $request->getContent());

        if ($requestParameters[0] === "" || count($requestParameters) < 2) {
            return null;
        }

        $parameters = [];

        foreach ($requestParameters as $requestParameter) {
            $exploded = explode('=', $requestParameter);
            $parameters[$exploded[0]] = $exploded[1];
        }

        return $parameters;
    }

    private function createQrScanner(string $deviceId)
    {
        $qrScanner = new QrScanner();
        $qrScanner->setName('Unassigned')
            ->setDeviceId($deviceId);

        $em = $this->getDoctrine()->getManager();
        $em->persist($qrScanner);
        $em->flush();
    }

    private function createQrScannerUserTracking(QrScanner $qrScanner, ?UserQuiz $userQuiz, ?User $user, $isGranted = false,  $vaccinValidity = false)
    {
        $qrScannerUserTracking = new QrScannerUserTracking();
        $qrScannerUserTracking->setIsGranted($isGranted)
            ->setQrScannerName($qrScanner->getName())
            ->setQrScannerAddress($qrScanner->getAddress())
            ->setQrScannerType($qrScanner->getType())
            ->setQrScanner($qrScanner)
            ->setUserQuiz($userQuiz)
            ->setCompany($qrScanner->getCompany())
            ->setIsVaccinationQrValid($vaccinValidity);

        if ($user) {
            $qrScannerUserTracking->setUserEmail($user->getEmail())
                ->setUserFirstName($user->getFirstName())
                ->setUserLastName($user->getLastName())
                ->setUserIndicator($user->getIndicator())
                ->setUserPhone($user->getPhone())
                ->setUserGroup($user->getGroup())
                ->setUser($user)
                ->setQuizStatus($user->getQuizStatus());

            if ($qrScanner->getQrScannerUserGroups()->contains($user->getGroup()) || $qrScanner->getContainsAllUsers()) {
                $qrScannerUserTracking->setScannerExistGroup(true);
            } else {
                $qrScannerUserTracking->setScannerExistGroup(false);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($qrScannerUserTracking);
        $em->flush();

        return $qrScannerUserTracking;
    }

    private function updateQrScannerUserTracking(QrScannerUserTracking $qrScannerUserTracking, QrScanner $qrScanner, ?UserQuiz $userQuiz, ?User $user, $tabletteQuizResponse, $isGranted = false) {
        // Setting the QrScannerUserTracking with the TabletteQuizResponseId (to be able to find the quiz corresponding to this QrScannerUserTracking)
        $qrScannerUserTracking->setIdTabletteQuizResponse($tabletteQuizResponse->getId());

        $qrScannerUserTracking->setIsGranted($isGranted);
        if ($user) {
            $qrScannerUserTracking->setUserQuiz($userQuiz)
                ->setUser($user)
                ->setUserGroup($user->getGroup());

            if ($qrScanner->getQrScannerUserGroups()->contains($user->getGroup()) || $qrScanner->getContainsAllUsers()) {
                $qrScannerUserTracking->setScannerExistGroup(true);
            } else {
                $qrScannerUserTracking->setScannerExistGroup(false);
            }
        }
      
        $this->em->persist($qrScannerUserTracking);
        $this->em->flush();
    }

    private function isGrantedUser(QrScanner $qrScanner, ?User $user, CompanySetting $companySetting, $isVaccinationQr, $vaccinValidity)
    {
        if ($qrScanner->getType() === 'in') {
            if (!$user || $user->getIsBlacklisted()) return false;

            if ($companySetting->getValidQuizRequiredQrScanner() && $user->getQuizStatus() !== User::QUIZ_STATUS_OK) return false;
        }

        // if its a vaccination QR code, we check the validity of the vaccination QR code
        if ($isVaccinationQr && $companySetting->getIsEnableModePandemic() && $vaccinValidity == false) return false;
        // if it is a vaccination QR code but the Pandemic Mode is DISABLED, we revoke the access
        if ($isVaccinationQr && !$companySetting->getIsEnableModePandemic()) return false;
        // if it is NOT a vaccination QR code but the Pandemic mode is ENABLED, we reboke the access
        // TODO Une fois qu'on va avoir implementer la logique pour permettre au user de televerser son code QR, il faudra
        // TODO regarder si la variable isVaccinationQrValid de l<entiter User est valide...
        if (!$isVaccinationQr && $companySetting->getIsEnableModePandemic()) return false;

        if ($qrScanner->getContainsAllUsers()) return true;

        if (!$qrScanner->getQrScannerUserGroups()->contains($user->getGroup())) {
            return false;
        }

        return true;
    }

    private function linkUserWithTabletteQuizResponse(User $user, $tabletteQuizResponse, $qrScannerUserTracking, UserQuizService $userQuizService, $pairedTablet, QuizRepository $quizRepository, GroupRepository $groupRepository)
    {
        $group = $groupRepository->findOneBy(['id' => $tabletteQuizResponse->getIdUserGroup()]);
        $user->setGroup($group);
        $this->em->persist($user);
        $this->em->flush();

        // Creating the User's Quiz in order to be able to expire the quiz and display it
        $userQuiz = $userQuizService->createSanitaryAndComplementaryUserQuizzes($user, $user->getCompany(), $user->getGroup(), $quizRepository);
        $userQuizService->determineUserQuizExpirationDate($userQuiz);
        $userQuiz->setAnsweredAt(new \DateTime());
        $userQuiz->setToken(null);

        // if the quiz is valid, we set the UserQuiz to valid
        if ($pairedTablet->getTabletteCompliantQuiz()) {
            $userQuiz->setGoodAnswers(1);
            $userQuiz->setWrongAnswers(0);
            $userQuiz->setisValid(1);
        } else {
            $userQuiz->setGoodAnswers(0);
            $userQuiz->setWrongAnswers(1);
            $userQuiz->setisValid(0);
        }

        $this->em->persist($userQuiz);
        $this->em->flush();

        // We modify the tabletteQuizResponseAnjoute Entity to link the Quiz with the User but also with the QrScannerUserTracking
        $tabletteQuizResponse->setIdReporting($qrScannerUserTracking->getId());
        $tabletteQuizResponse->setIdUser($qrScannerUserTracking->getUser()->getId());
        $tabletteQuizResponse->setIdUserQuiz($userQuiz->getId());

        $this->em->merge($tabletteQuizResponse);
        $this->em->flush();
    }

    private function updateQuizStatusPairedTablet($quizValidity = false, User $user)
    {
        // We update the User's quiz status according to the validity of the quiz
        if ($quizValidity) {
            $user->setQuizStatus(User::QUIZ_STATUS_OK);
        } else {
            $user->setQuizStatus(User::QUIZ_STATUS_KO);
        }
        $this->em->persist($user);
        $this->em->flush();
    }

    private function createUserUsingVaccinQR($groupe, $firstName, $lastName, $company, $birthdate)
    {
        $user = new User();
        $user->setGroup($groupe);
        $user->setEmail("userWithNoEmail@ssca.com");
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setCompany($company);
        $user->setRoles(['ROLE_ANONYMOUS']);
        $user->setBirthDateVaccinCode($birthdate);
        $user->setIndicator('+1');
        $user->setQuizStatus(User::QUIZ_STATUS_NOT_ANSWERED);
        $user->setContactLanguage('fr');

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    private function sendEmailSubscribersQrScan($isGranted, $userQuizGroupListenerRepository, $groupe, $companySetting, $emailService, User $user, QrScanner $qrScanner)
    {
        if ($isGranted) {
            // send COMPLIANT Qr scan subscriber email
            $usersListening = $userQuizGroupListenerRepository->findBy(['receiveSuccessQrScan' => true, 'groupe' => $groupe]);

            foreach ($usersListening as $userListening) {
                $tempUser = $userListening->getUser();

                if ($tempUser->getPasswordToken() === null) {
                    $subscribers[] = $tempUser;
                }
            }
            if (isset($subscribers)) {
                if ($companySetting->getEmailSuccessQrScanSubscribers() && $companySetting->getAllowSendEmail()) {
                    $emailService->sendSuccessQrScanSubscribers($subscribers, $user, $qrScanner);
                }
            }
        } else {
            // send NON-COMPLIANT Qr scan subscriber email
            $usersListening = $userQuizGroupListenerRepository->findBy(['receiveErrorQrScan' => true, 'groupe' => $groupe]);

            foreach ($usersListening as $userListening) {
                if ($userListening->getUser()->getPasswordToken() === null) {
                    $subscribers[] = $userListening->getUser();
                }
            }
            if (isset($subscribers)) {
                if ($companySetting->getEmailErrorQrScanSubscribers() && $companySetting->getAllowSendEmail()) {
                    $emailService->sendErrorQrScanSubscribers($subscribers, $user, $qrScanner);
                }
            }
        }
    }
}
